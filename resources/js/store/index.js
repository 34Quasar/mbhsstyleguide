import Vue from 'vue';
import Vuex from 'vuex';
//import authModule from './modules/auth';
import pageModule from './modules/page';

Vue.use(Vuex)

const store = new Vuex.Store({
  modules:{
   //auth:authModule,
   page:pageModule
  }
});
export default store;